//
//  ViewController.swift
//  Cat Years
//
//  Created by Paul Pearson on 2/2/16.
//  Copyright © 2016 RPM Consulting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var catYears: UILabel!
    
    @IBOutlet var catAgeTextField: UITextField!
    
    @IBAction func findAgeButton(sender: AnyObject) {
        
        var catAge = Int(catAgeTextField.text!)!
        catAge = catAge * 7
        catYears.text = "Your cat is \(catAge) in cat years"
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

